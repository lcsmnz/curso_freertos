/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "string.h"
#include "timers.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef struct
{
	uint8_t payload[10];
	uint32_t len;
}command_t;


typedef enum
{
	sMainMenu =0,
	sLedEffect,
	sRTCMenu,
	sRTCTimeConfig,
	sRTCDateConfig,
	sRTCReport,
}State_t;

extern State_t curr_state;

extern TaskHandle_t hTaskMenu;
extern TaskHandle_t hTaskLED;
extern TaskHandle_t hTaskRTC;
extern TaskHandle_t hTaskPrint;
extern TaskHandle_t hTaskCMD;

extern TimerHandle_t hLEDTimer[4];

extern QueueHandle_t q_data;
extern QueueHandle_t q_print;

extern UART_HandleTypeDef huart2;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void TaskMenu(void* parameters);
void TaskLED(void* parameters);
void TaskRTC(void* parameters);
void TaskPrint(void* parameters);
void TaskCMD(void* parameters);

void led_effect_stop(void);
void led_effect(uint8_t effect);


void led_effect1(void);
void led_effect2(void);
void led_effect3(void);
void led_effect4(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LD4_Pin GPIO_PIN_12
#define LD4_GPIO_Port GPIOD
#define LD3_Pin GPIO_PIN_13
#define LD3_GPIO_Port GPIOD
#define LD5_Pin GPIO_PIN_14
#define LD5_GPIO_Port GPIOD
#define LD6_Pin GPIO_PIN_15
#define LD6_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

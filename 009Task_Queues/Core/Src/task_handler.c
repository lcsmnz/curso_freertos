/*
 * task_handler.c
 *
 *  Created on: May 28, 2021
 *      Author: Lucas Muniz
 */




#include "main.h"

int extract_command(command_t *cmd);
void process_command(command_t *cmd);
const char* msg_inv = "/////INVALID OPTION/////\n";


void TaskMenu(void* parameters)
{
	uint32_t cmd_addr;
	uint8_t option;
	command_t *cmd;
	const char* msg_menu ="========================\n"
						  "|         MENU         |\n"
						  "========================\n"
						  "LED Effect  -------->0  \n"
			              "DATE and TIME ------>1  \n"
		                  "EXIT --------------->2  \n"
			 	 	 	  "Enter your choice here:   ";
	while(1)
	{
		xQueueSend(q_print,&msg_menu,portMAX_DELAY);

		xTaskNotifyWait(0,0,&cmd_addr, portMAX_DELAY);

		cmd = (command_t*)cmd_addr;

		if(cmd->len==1)
		{
			option =cmd->payload[0]-48;
			switch (option)
			{
				case 0:
					curr_state = sLedEffect;
					xTaskNotify(hTaskLED,0, eNoAction);
				break;

				case 1:
					curr_state = sRTCMenu;
					xTaskNotify(hTaskRTC,0, eNoAction);
				break;

				case 2: //IMPLEMENT EXIT

				break;

				default:
					//invalid entry
					xQueueSend(q_print,&msg_inv,portMAX_DELAY);
				continue;
			}
		}
		else
		{
			//invalid entry
			xQueueSend(q_print,&msg_inv,portMAX_DELAY);
		}

		xTaskNotifyWait(0,0,NULL, portMAX_DELAY);
	}

}

void TaskLED(void* parameters)
{

	uint32_t cmd_addr;
	command_t *cmd;
	const char* msg_led = "========================\n"
						  "|      LED Effect      |\n"
						  "========================\n"
						  "(none,e1,e2,e3,e4)\n"
						  "Enter your choice here : ";

	while(1){
		/*Wait for notification (Notify wait) */
		xTaskNotifyWait(0,0,NULL, portMAX_DELAY);
		/* Print LED menu */
		xQueueSend(q_print,&msg_led,portMAX_DELAY);
		/* wait for LED command (Notify wait) */
		xTaskNotifyWait(0,0,&cmd_addr,portMAX_DELAY);
		cmd=(command_t*)cmd_addr;
		if(cmd->len <= 4)
		{
			if(! strcmp((char*)cmd->payload,"none"))
			{
				led_effect_stop();
			}
			else if (! strcmp((char*)cmd->payload,"e1"))
			{
			led_effect(1);
			}
			else if (! strcmp((char*)cmd->payload,"e2"))
			{
				led_effect(2);
			}
				else if (! strcmp((char*)cmd->payload,"e3"))
			{
				led_effect(3);
			}
			else if (! strcmp((char*)cmd->payload,"e4"))
			{
				led_effect(4);
			}
			else
			{
				/*print invalid message */
				xQueueSend(q_print,&msg_inv,portMAX_DELAY);
			}
		}
		else
		{
			/* print invalid message */
			xQueueSend(q_print,&msg_inv,portMAX_DELAY);
		}
		/*update state variable */
		curr_state = sMainMenu;

		/*Notify menu task */
		xTaskNotify(hTaskMenu,0,eNoAction);

	}

}

void TaskRTC(void* parameters)
{

	while(1)
	{

	}
}

void TaskPrint(void* parameters)
{
	uint32_t *msg;
	while(1)
	{
		xQueueReceive(q_print, &msg, portMAX_DELAY);
		HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen((char*)msg), HAL_MAX_DELAY);
	}
}

void TaskCMD(void* parameters)
{
	BaseType_t ret;
	command_t cmd;
	while(1)
	{
		ret=xTaskNotifyWait(0,0,NULL, portMAX_DELAY);
		if(ret==pdTRUE)
		{
			process_command(&cmd);
		}
	}
}


void process_command(command_t *cmd)
{
	extract_command(cmd);

	switch (curr_state)
	{
		case (sMainMenu):
			xTaskNotify(hTaskMenu, (uint32_t)cmd, eSetValueWithOverwrite);
		break;

		case (sLedEffect):
			xTaskNotify(hTaskLED, (uint32_t)cmd, eSetValueWithOverwrite);
		break;

		case (sRTCMenu):
			xTaskNotify(hTaskRTC, (uint32_t)cmd, eSetValueWithOverwrite);
		break;

		case (sRTCTimeConfig):
			xTaskNotify(hTaskRTC, (uint32_t)cmd, eSetValueWithOverwrite);
		break;

		case (sRTCDateConfig):
			xTaskNotify(hTaskRTC, (uint32_t)cmd, eSetValueWithOverwrite);
		break;

		case (sRTCReport):
			xTaskNotify(hTaskRTC, (uint32_t)cmd, eSetValueWithOverwrite);
		break;
	}
}

int extract_command(command_t *cmd)
{
	uint8_t item;
	BaseType_t status;
	uint8_t i=0;

	status = uxQueueMessagesWaiting(q_data);
	if(!status) return -1;

	do
	{
		status = xQueueReceive(q_data, &item, 0);
		if(status == pdTRUE)
		{
			cmd->payload[i++] = item;
		}
	}while(item != '\n');

	cmd->payload[i-1]='\0';
	cmd->len = i-1;

	return 0;
}


/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "task.h"
#include "stdio.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//uint32_t LD1_TICK=0, LD2_TICK=0, LD3_TICK=0, LD4_TICK=0, LD_TICK=0;
TaskHandle_t hNextTask = NULL;

TaskHandle_t hTask1;
TaskHandle_t hTask2;
TaskHandle_t hTask3;
TaskHandle_t hTask4;
//TaskHandle_t hTaskButton;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
static void LED1_handler(void* parameters);
static void LED2_handler(void* parameters);
static void LED3_handler(void* parameters);
static void LED4_handler(void* parameters);
extern void button_interrupt_handler(void);
//static void Button_handler(void* parameters);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define DWT_CTRL (*(volatile uint32_t*)0xE0001000)
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

	BaseType_t status;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  DWT_CTRL |= (1 << 0);
  SEGGER_SYSVIEW_Conf();
  SEGGER_SYSVIEW_Start();

  status = xTaskCreate(LED1_handler, "TASK LED1", 200, NULL, 1, &hTask1);
  configASSERT(status== pdPASS);

  hNextTask = hTask1;

  status = xTaskCreate(LED2_handler, "TASK LED2", 200, NULL, 2, &hTask2);
  configASSERT(status== pdPASS);

  status = xTaskCreate(LED3_handler, "TASK LED3", 200, NULL, 3, &hTask3);
  configASSERT(status== pdPASS);

 status = xTaskCreate(LED4_handler, "TASK LED4", 200, NULL, 4, &hTask4);
 configASSERT(status== pdPASS);
/*
  status = xTaskCreate(Button_handler, "TASK Button", 200, NULL, 5, &hTaskButton);
  configASSERT(status== pdPASS);
*/
  //start FreeRTOS scheduler
  vTaskStartScheduler(); //
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
static void LED1_handler(void* parameters)
{
	BaseType_t status;

	while(1)
	{
		HAL_GPIO_TogglePin(GPIOD, LD3_Pin);
		//vTaskDelayUntil(&LastWakeUptime,pdMS_TO_TICKS(510) );
		status = xTaskNotifyWait(0, 0,NULL,pdMS_TO_TICKS(1000));
		if(status == pdTRUE)
		{
			portENTER_CRITICAL();
			//vTaskSuspendAll();
			hNextTask = hTask2;
			//xTaskResumeAll();
			HAL_GPIO_WritePin(GPIOD, LD3_Pin, GPIO_PIN_SET);
			portEXIT_CRITICAL();
			vTaskDelete(NULL);
		}

	}
}

static void LED2_handler(void* parameters)
{
	BaseType_t status;

	while(1)
	{
		HAL_GPIO_TogglePin(GPIOD, LD4_Pin);
		//vTaskDelayUntil(&LastWakeUptime,pdMS_TO_TICKS(515) );
		status = xTaskNotifyWait(0, 0,NULL,pdMS_TO_TICKS(800));
		if(status == pdTRUE)
		{
			portENTER_CRITICAL();
			//vTaskSuspendAll();
			hNextTask = hTask3;
			//xTaskResumeAll();
			HAL_GPIO_WritePin(GPIOD, LD4_Pin, GPIO_PIN_SET);
			portEXIT_CRITICAL();
			vTaskDelete(NULL);
		}
	}
}

static void LED3_handler(void* parameters)
{
	BaseType_t status;

	while(1)
	{
		HAL_GPIO_TogglePin(GPIOD, LD5_Pin);
		//vTaskDelayUntil(&LastWakeUptime,pdMS_TO_TICKS(505) );
		status = xTaskNotifyWait(0, 0,NULL,pdMS_TO_TICKS(600));
		if(status == pdTRUE)
		{
			portENTER_CRITICAL();
			//vTaskSuspendAll();
			hNextTask = hTask4;
			//xTaskResumeAll();
			HAL_GPIO_WritePin(GPIOD, LD5_Pin, GPIO_PIN_SET);
			portEXIT_CRITICAL();
			vTaskDelete(NULL);
		}
	}
}

static void LED4_handler(void* parameters)
{
	BaseType_t status;
	while(1)
	{
		HAL_GPIO_TogglePin(GPIOD, LD6_Pin);
		//vTaskDelayUntil(&LastWakeUptime,pdMS_TO_TICKS(500) );
		status = xTaskNotifyWait(0, 0,NULL,pdMS_TO_TICKS(1200));
		if(status == pdTRUE)
		{
			portENTER_CRITICAL();
			//vTaskSuspendAll();
			hNextTask = NULL;
			//xTaskResumeAll();
			HAL_GPIO_WritePin(GPIOD, LD6_Pin, GPIO_PIN_SET);
			//vTaskDelete(hTaskButton);
			portEXIT_CRITICAL();
			vTaskDelete(NULL);
		}
	}
}

extern void button_interrupt_handler(void)
{
	traceISR_ENTER();
	xTaskNotifyFromISR(hNextTask, 0, eNoAction, NULL);
	traceISR_EXIT();
}

/*
static void Button_handler(void* parameters)
{
	uint8_t Btn_read = 0;
	uint8_t Prev_read = 0;

	while(1)
	{
		Btn_read = HAL_GPIO_ReadPin(GPIOA, Button_Pin);

		if(Btn_read)
		{
			if(!Prev_read)
			{
				xTaskNotify(hNextTask,0,eNoAction);
			}

		}
		Prev_read = Btn_read;
		vTaskDelay(pdMS_TO_TICKS(10));
	}


}
*/

/* USER CODE END 4 */

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
